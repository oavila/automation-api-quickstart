https://about.gitlab.com

[gitlab web](https://about.gitlab.com)

[Referencia al archivo](README.md) (README.md)

IN-LINE STYLE:
![Gitlab logo](gitlab-security-icon-rgb.png)

REFERENCE STYLE:
![Gitlab logo2][logo]

[logo]:gitlab-security-icon-rgb.png

``` python
def function():
    #Indenting words jost fine in the face code block
    s = "python syntax highlighthing"
    print s
```
> Hola esto es un quote
> Esto también es parte del quote

@oavila
@all
#1
e3a24277
%1.0

:sunglasses:
