Cabecera 1
==========

Cabecera 2
----------

# Cabecera 1
## Cabecera 2
### Cabecera 3
#### Cabecera 4

**GitLab** es una *potente* solución de software libre que te va a permitir a crear y gestionar repositorios de código y documentos. Con GitLab podrás crear un servicio en tu propio servidor o utilizando los servicios que te ofrece para gestionar con facilidad cambios y versiones de tus ficheros o de los ficheros de otras personas. Especialmente utilizado por equipos de desarrolladores de aplicaciones cuando están repartidos a lo largo del mundo, en este curso verás tanto el papel del administrador como el del usuario, para la configuración y la gestión de repositorios para IT usando GitLab.

## Listas

1. Uno
2. Dos
3. Tres
4. Cuatro
    - Uno
    - Dos
    * Tres
    * Cuatro

* Uno
* Dos
* Tres
* Cuatro

:poop:
